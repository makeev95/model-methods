package util;

/*критерии прекращения поиска*/
public enum StopCondition {
    ACCURACY_FUNC,
    AMOUNT_OF_LOOPS,
    AMOUNT_OF_FUNC_CALC
}
