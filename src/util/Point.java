package util;

public class Point {
    private double y1;
    private double y2;
    private double val;

    Point(double y1, double y2, double val) {
        this.y1 = y1;
        this.y2 = y2;
        this.val = val;
    }

    public double getY1() {
        return y1;
    }

    public double getY2() {
        return y2;
    }

    public double getVal() {
        return val;
    }
}
